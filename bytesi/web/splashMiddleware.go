package web

import (
	"context"
	"gitlab.com/pcanilho/challenges/bytesi/db"
	"gitlab.com/pcanilho/challenges/bytesi/db/models"
	"net"
	"net/http"
	"strings"
)

func splashMiddleware(driver *db.Driver) func (h http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ip, _, _ := net.SplitHostPort(r.RemoteAddr)
			session := models.Session {
				Address:  ip,
			}
			if userSession, found := driver.SessionExists(&session); !found {
				if r.RequestURI != "/welcome" &&
					!strings.HasPrefix(r.RequestURI, "/external") {
					http.Redirect(w, r, "/welcome", http.StatusMovedPermanently)
				}
			} else {
				r = r.WithContext(context.WithValue(r.Context(), "session", userSession))
			}
			h.ServeHTTP(w, r)
		})
	}
}
