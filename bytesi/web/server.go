package web

import (
	"context"
	"fmt"
	"github.com/gorilla/mux"
	logger "github.com/sirupsen/logrus"
	"gitlab.com/pcanilho/challenges/bytesi/db"
	dbModels "gitlab.com/pcanilho/challenges/bytesi/db/models"
	"gitlab.com/pcanilho/challenges/bytesi/models"
	"html/template"
	"net"
	"net/http"
	"regexp"
	"strings"
	"time"
)

type Server struct {
	serverInstance 	*http.Server
	contextLogger 	*logger.Entry
	config			*models.Config
	tlsConfig		*models.TLSOpts
	dbDriver		*db.Driver
}

func NewServer(config *models.Config, tlsConfig *models.TLSOpts, dbDriver *db.Driver, logger *logger.Entry) *Server {
	r := mux.NewRouter()
	instance := &http.Server{
		Addr:              fmt.Sprintf("%s:%d", config.Web.Listen, config.Web.Port),
		Handler:           r,
	}
	serverInstance := &Server{
		serverInstance: instance,
		contextLogger: 	logger,
		config: 		config,
		tlsConfig: 		tlsConfig,
		dbDriver: 		dbDriver,
	}

	r.Use(splashMiddleware(dbDriver))
	r.HandleFunc("/", serverInstance.HomePageHandler).
		Methods(http.MethodGet)
	r.HandleFunc("/welcome", serverInstance.WelcomeHandler).
		Methods(http.MethodGet)
	r.HandleFunc("/login", serverInstance.LoginHandler).
		Methods(http.MethodGet, http.MethodPost)
	r.HandleFunc("/flags", serverInstance.FlagsHandler)
	r.HandleFunc("/flag{number:[0-9]}", serverInstance.FlagsHandler).
		Methods(http.MethodPost)
	r.HandleFunc("/session", serverInstance.SessionsHandler).
		Methods(http.MethodPost)
	r.HandleFunc("/api/{method}", serverInstance.QueryHandler).
		Methods(http.MethodPost)
	r.PathPrefix("/external/static").
		Handler(http.StripPrefix("/external/static", http.FileServer(http.Dir(config.Web.Static)))).
		Methods(http.MethodGet)


	logger.Debugf("Creating instance...")
	return serverInstance
}

func (s *Server) Start(context context.Context, cancelFunc context.CancelFunc) {
	go func() {
		select {
		case <-context.Done():
			s.contextLogger.Info("Server shutting down...")
			return
		default:
			<- time.After(time.Second*5)
		}
	}()

	go func() {
		err := s.serverInstance.ListenAndServeTLS(
			s.tlsConfig.CertificatePath, s.tlsConfig.CertificateKey)
		if err != nil { cancelFunc() }
	}()
}

func (s *Server) WelcomeHandler(w http.ResponseWriter, r *http.Request) {
	ip, _, _ := net.SplitHostPort(r.RemoteAddr)
	session := dbModels.Session{
		Address:  ip,
	}
	if _, found := s.dbDriver.SessionExists(&session); found {
		http.Redirect(w, r, "/login", http.StatusPermanentRedirect)
	}

	t, err := template.ParseFiles("web/templates/splash.gohtml")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	err = t.Execute(w, nil)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (s *Server) HomePageHandler(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/login", http.StatusPermanentRedirect)
	return
}

func (s *Server) LoginHandler(w http.ResponseWriter, r *http.Request) {
	foundSession := r.Context().Value("session")
	if foundSession == nil {
		s.WelcomeHandler(w, r)
		return
	}
	currentSession := foundSession.(*dbModels.Session)

	t, err := template.ParseFiles("web/templates/login.gohtml", "web/templates/navbar.gohtml")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		logger.Fatal(err)
	}

	contextOutput := r.Context().Value("output")
	if contextOutput != nil {
		if v, valid := contextOutput.([]uint8); valid {
			contextOutput = string(v)
		} else {
			contextOutput = fmt.Sprintf("%v", contextOutput)
		}
	}
	data := struct {
		Session *dbModels.Session
		Output interface{}
	}{currentSession, contextOutput}
	if err = t.Execute(w, data); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		logger.Fatal(err)
	}
}

func (s *Server) SessionsHandler(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		s.contextLogger.Errorf("Unable to parse the [session] post. Error [%v]", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	fName, found := r.PostForm["name"]
	if !found {
		s.contextLogger.Errorf(`Bad [session] form. The "name" field must exist.`)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	ip, _, _ := net.SplitHostPort(r.RemoteAddr)
	if err := s.dbDriver.CreateSession(&dbModels.Session{
		Name: fName[0],
		Address:  ip,
	}); err != nil {
		s.contextLogger.Errorf("Failed to create the session for the user [%s]. Error [%v]",
			fName, err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	s.contextLogger.Infof("New session created for user [%v] and IP [%v]", fName[0], ip)
	http.Redirect(w, r, "/login", http.StatusPermanentRedirect)
}


func (s *Server) FlagsHandler(w http.ResponseWriter, r *http.Request) {
	foundSession := r.Context().Value("session")
	if foundSession == nil {
		s.WelcomeHandler(w, r)
		return
	}
	currentSession := foundSession.(*dbModels.Session)

	t, err := template.New("").Funcs(template.FuncMap{
		"FlagName": dbModels.FlagName,
		"IsFlagCaptured": dbModels.IsFlagCaptured,
		"GameOver": dbModels.GameOver,
	}).ParseFiles("web/templates/flags.gohtml", "web/templates/navbar.gohtml")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		logger.Fatal(err)
	}
	if regexp.MustCompile("flag[0-9]").MatchString(r.RequestURI) {
		if err = r.ParseForm(); err != nil {
			s.contextLogger.Error("Unable to parse the flagNumber form. Error [%v]", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		flagNumber := strings.Split(r.RequestURI, "flag")[1]
		flagValue, foundVal := r.PostForm["flag"]
		if !foundVal {
			s.contextLogger.Error("Unable to parse the flagNumber form. Error [%v]", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		flagTitle := fmt.Sprintf("Flag %s", flagNumber)
		if flag, isCaptured := s.dbDriver.IsCapturedFlag(&dbModels.Flag{
			Title: flagTitle,
			Value: flagValue[0],
		}); isCaptured {
			// Session won the flag
			if err = s.dbDriver.CapturedFlag(currentSession, flag); err != nil {
				s.contextLogger.Errorf("Failed to capture the flag for session [%v] and flag [%v]. Error [%v]",
					currentSession.Name, flagTitle, err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
		}
	}

	if err = t.ExecuteTemplate(w, "flags", struct {
		Flags []dbModels.Flag
		Session *dbModels.Session
	}{s.config.Flags, currentSession}); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		logger.Fatal(err)
	}
}

func (s *Server) QueryHandler(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		s.contextLogger.Error("Unable to parse the [login] form. Error [%v]", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	username, password := r.PostForm["u"][0], r.PostForm["p"][0]
	sqlQuery := fmt.Sprintf(`SELECT name FROM accounts where name = '%s' and password like '%s'`,
		username, password)
	logger.Tracef("Executing query [%v]...", sqlQuery)
	result := s.dbDriver.AttemptLogin(sqlQuery)
	s.contextLogger.Tracef("Processing query output...")
	var row interface{}
	if err := result.Scan(&row); err != nil {
		row = err
	}
	r = r.WithContext(context.WithValue(r.Context(), "output", row))
	r.RequestURI = "/login"
	s.LoginHandler(w, r)
}
