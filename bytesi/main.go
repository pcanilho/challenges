package main

import (
	"context"
	nested "github.com/antonfisher/nested-logrus-formatter"
	logger "github.com/sirupsen/logrus"
	"gitlab.com/pcanilho/challenges/bytesi/config"
	"gitlab.com/pcanilho/challenges/bytesi/db"
	"gitlab.com/pcanilho/challenges/bytesi/models"
	"gitlab.com/pcanilho/challenges/bytesi/settings"
	"gitlab.com/pcanilho/challenges/bytesi/web"
	"os"
	"time"
)

var appArgs settings.AppArgs

func main() {
	// 1. Handle args
	logLevel, err := settings.ParseApplicationSettings(&appArgs, os.Args[1:])
	if err != nil {	panic(err) }

	// 2. Logger setup
	logger.SetLevel(logLevel)
	logger.SetFormatter(&nested.Formatter{
		ShowFullLevel: 		true,
		TimestampFormat:	time.RFC3339,
	})

	// 3. Load config
	log := logger.WithField("context", "configuration")
	log.Debugf("Attempting to parse the configuration file [%s]...", appArgs.Config)
	cfg, err := config.ParseConfig(appArgs.Config)
	if err != nil {
		log.Errorf("Unable to parse the configuration file [%s]. Error [%v]", appArgs.Config, err)
		return
	}

	// 4. Initialize database connection
	log = logger.WithField("context", "database")
	log.Debugf("Attempting to connect to the database [%s:%d] with user [%s]...",
		cfg.Sql.Location, cfg.Sql.Port, cfg.Sql.Credentials.Name)
	dbDriver, err := db.NewDriver(cfg.Sql, cfg.Flags)
	if err != nil {
		log.Errorf("Unable to connect to the database [%s:%d] with user [%s]. Error [%v]",
			cfg.Sql.Location, cfg.Sql.Port, cfg.Sql.Credentials.Name, err)
	}
	if err = dbDriver.Initialise(); err != nil {
		log.Errorf("Unable to initialise the database. Error [%v]", err)
		return
	}

	// 5. Create web views
	log = logger.WithField("context", "website")
	log.Debugf("Attempting to start the webserver...")
	server := web.NewServer(cfg, &models.TLSOpts{
		CertificatePath: appArgs.CertificateFile,
		CertificateKey:  appArgs.CertificateKey,
	}, dbDriver, log)
	ctx, cancelFunc := context.WithCancel(context.Background())
	server.Start(ctx, cancelFunc)
	log.Infof("Web server started: https://localhost:%d", cfg.Web.Port)


	// 6. Handle SQL queries
	select {}
}
