package db

import (
	"database/sql"
	"fmt"
	"github.com/pkg/errors"
	dbModels "gitlab.com/pcanilho/challenges/bytesi/db/models"
	"gitlab.com/pcanilho/challenges/bytesi/models"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"strings"
)

type Driver struct {
	cfg 	*models.DBOpts
	flags 	[]dbModels.Flag
	db 		*gorm.DB
}

func NewDriver(cfg *models.DBOpts, flags []dbModels.Flag) (*Driver, error) {
	instance := &Driver{cfg: cfg, flags: flags}
	if err := instance.connect(); err != nil {
		return nil, err
	}
	return instance, nil
}


func (d *Driver) connect() (err error) {
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		d.cfg.Credentials.Name, d.cfg.Credentials.Password, d.cfg.Location, d.cfg.Port, d.cfg.Database)

	if d.db, err = gorm.Open(mysql.Open(dsn), &gorm.Config{}); err != nil {
		return err
	}
	return
}

func (d *Driver) Initialise() error {
	if err := d.db.AutoMigrate(&dbModels.Account{}); err != nil {
		return errors.Wrap(err, "Failed to create the [Account] table")
	}
	if err := d.db.AutoMigrate(&dbModels.Flag{}); err != nil {
		return errors.Wrap(err, "Failed to create the [Flag] table")
	}
	if err := d.db.AutoMigrate(&dbModels.Session{}); err != nil {
		return errors.Wrap(err, "Failed to create the [Sessions] table")
	}

	for _, t := range d.cfg.Schema.Tables {
		if strings.ToLower(t.Name) == "users" {
			for _, u := range t.Data {
				if err := d.db.FirstOrCreate(&u, "name = ?", u.Name).Error; err != nil {
					return errors.Wrap(err, "failed to create users")
				}
			}
		}
	}
	for _, f := range d.flags {
		if err := d.db.FirstOrCreate(&f, "title = ?", f.Title).Error; err != nil {
			return errors.Wrap(err, "failed to create flags")
		}
	}
	return nil
}

func (d *Driver) AttemptLogin(sqlQuery string) *sql.Row {
	return d.db.Raw(sqlQuery).Row()
}

func (d *Driver) IsCapturedFlag(flag *dbModels.Flag) (*dbModels.Flag, bool) {
	var out dbModels.Flag
	d.db.
		Where("title = ?", flag.Title).
		Find(&out)
	return &out, out.Value == flag.Value
}

func (d *Driver) CapturedFlag(session *dbModels.Session, flag *dbModels.Flag) error {
	var out dbModels.Flag
	d.db.
		Where("title = ?", flag.Title).
		Find(&out)
	flags := append(session.CapturedFlags, *flag)

	return d.db.Model(session).UpdateColumn("CapturedFlags", flags).Error
}

func (d *Driver) SessionExists(session *dbModels.Session) (*dbModels.Session, bool) {
	var out dbModels.Session
	d.db.Preload("CapturedFlags").
		Where("address = ?", session.Address).
		Find(&out)
	return &out, len(out.Name) > 0
}

func (d *Driver) CreateSession(session *dbModels.Session) error {
	return d.db.Create(&session).Error
}
