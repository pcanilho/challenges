package models

import "gorm.io/gorm"

type Account struct {
	gorm.Model
	ID   		uint64	// AUTO INCREMENTED
	Name		string	`gorm:"not null"`
	Password	string	`gorm:"not null"`
}
