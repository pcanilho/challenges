package models

import (
	"gorm.io/gorm"
	"strings"
)

type Flag struct {
	gorm.Model
	ID   	uint64	// AUTO INCREMENTED
	Title	string	`gorm:"not null"`
	Value	string	`gorm:"not null"`
	Hint    string  `gorm:"not null"`
}

func FlagName(flag Flag) string {
	return strings.ToLower(strings.ReplaceAll(flag.Title, " ", ""))
}
