package models

import "gorm.io/gorm"

type Session struct {
	gorm.Model
	ID            uint64 // AUTO INCREMENTED
	Address       string `gorm:"not null"`
	Name          string `gorm:"not null"`
	CapturedFlags []Flag `gorm:"many2many:session_flags"`
}

func IsFlagCaptured(flag Flag, session Session) bool {
	for _, f := range session.CapturedFlags {
		if f.Title == flag.Title {
			return true
		}
	}
	return false
}

func GameOver(session Session, flags []Flag) bool {
	return len(session.CapturedFlags) == len(flags)
}
