module gitlab.com/pcanilho/challenges/bytesi

go 1.16

require (
	github.com/antonfisher/nested-logrus-formatter v1.3.0
	github.com/gorilla/mux v1.8.0
	github.com/jessevdk/go-flags v1.4.0
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.8.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
	gorm.io/driver/mysql v1.0.4
	gorm.io/gorm v1.20.12
	github.com/go-sql-driver/mysql v1.5.0
)
