package config

import (
	"github.com/pkg/errors"
	"gitlab.com/pcanilho/challenges/bytesi/models"
	"gopkg.in/yaml.v3"
	"os"
)

func ParseConfig(path string) (*models.Config, error) {
	out := new(models.Config)
	b, err := os.ReadFile(path)
	if err != nil {
		return nil, errors.Wrapf(err, "unable to read the configuration file [%s]", path)
	}

	if err = yaml.Unmarshal(b, out); err != nil {
		return nil, errors.Wrapf(err, "unable to parse the [Config] struct from the file [%s]", path)
	}
	return out, err
}
