package settings

import (
	"fmt"
	"github.com/jessevdk/go-flags"
	logger "github.com/sirupsen/logrus"
	"os"
)

var (
	version, commit string
)

type AppArgs struct {
	Version			bool `long:"version" description:"The application version"`
	Verbose 		[]bool `short:"v" long:"verbose" description:"Verbosity level"`
	Config  		string `default:"config.yaml" short:"c" long:"config" description:"File path of the configuration file"`
	CertificateFile string `default:"localhost.crt" long:"certificate" description:"The path to the TLS certificate file"`
	CertificateKey 	string `default:"localhost.key" long:"certificate_key" description:"The path to the TLS certificate file"`
}

// ParseApplicationSettings : Helper function to handle the parsing of the @see AppArgs schema against a given slice of
// arguments in slice format
func ParseApplicationSettings(args *AppArgs, values []string) (logger.Level, error) {
	appSettingsParser := flags.NewParser(args, flags.HelpFlag|flags.PassDoubleDash)
	_, err := appSettingsParser.ParseArgs(values)
	if args.Version {
		fmt.Printf("v%s (%s)\n", version, commit)
		os.Exit(0)
	}
	if err != nil {
		appSettingsParser.WriteHelp(os.Stdout)
		if flagsErr, ok := err.(*flags.Error); ok && flagsErr.Type == flags.ErrHelp {
			os.Exit(0)
		} else {
			fmt.Println(err)
			os.Exit(1)
		}
	}

	// Apply verbosity level
	stdoutLevel := parseVerbosityLevel(args)
	return stdoutLevel, nil
}

func parseVerbosityLevel(args *AppArgs) logger.Level {
	if args.Verbose != nil && len(args.Verbose) > 0 {
		verbosityLevel := len(args.Verbose)
		switch verbosityLevel {
		case 1:
			return logger.InfoLevel
		case 2:
			return logger.DebugLevel
		case 3:
			return logger.TraceLevel
		}

		// Allow any given number of Vs
		if verbosityLevel > 3 {
			return logger.TraceLevel
		}
	}
	// Default to ERROR level if no flag has been provided
	return logger.ErrorLevel
}
