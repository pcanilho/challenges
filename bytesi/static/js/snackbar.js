function showHint(id) {
    let x = document.getElementById(id);
    x.className = "show";
    setTimeout(() => { x.className = x.className.replace("show", ""); }, 3000);
}
