package models

import "gitlab.com/pcanilho/challenges/bytesi/db/models"

type Table struct {
	Name string
	Data []models.Account
}

type Schema struct {
	Tables []Table
}

type DBOpts struct {
	Port        uint16
	Location    string
	Database    string
	Credentials models.Account
	Schema      Schema
}
