package models

type WebOpts struct {
	Listen 				string
	Port				uint16
	Static				string
}
