package models

type TLSOpts struct {
	CertificatePath, CertificateKey string
}
