package models

import dbModels "gitlab.com/pcanilho/challenges/bytesi/db/models"

type Config struct {
	Web *WebOpts
	Sql *DBOpts
	Flags []dbModels.Flag
}
