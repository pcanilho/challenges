module gitlab.com/pcanilho/security

go 1.15

require (
	github.com/antonfisher/nested-logrus-formatter v1.3.0 // indirect
	github.com/jessevdk/go-flags v1.4.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.8.0 // indirect
	gitlab.com/pcanilho/challenges/bytesi v0.0.0-20210226123620-8bd77feeaded // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
