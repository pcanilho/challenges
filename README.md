# Welcome to the `Argonauts` security challenges!

![image](http://www.greekexperience.gr/wp-content/uploads/2017/02/5050161218120012.jpg)


| Challenge Name | Type | Description  |
| ----------- | ----------- | ----------- |
| `bytesi`     | `SQL injection`       | This challenge consists of a web server that communicates to a MySQL database via an `ORM`. However, this connection might not be as secure as one would like. Oops! |

